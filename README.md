# About
A directory of open source projects and open license climbing datasets by the climbing community.  Submission welcome! (Please submit a PR).

# Topos
- Climbing topos for Tahquitz Rock  https://github.com/bcrowell/tahquitz

# GIS Data
- US climbing routes dataset https://gitlab.com/openbeta/opendata/tree/master/usa
- A scraper and reddit bot for the website MountainProject.com https://github.com/derekantrican/MountainProject

# Studies & Researches
- Climber Self-governance https://www.policyandadmin.org/climbers
